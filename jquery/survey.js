/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$("#field1").alpaca({
    "dataSource": "/data/customer-profile-data.json",
    "schemaSource": "/data/customer-profile-schema.json",
    "optionsSource": "/data/customer-profile-options.json",
    "view": {
        "parent": "bootstrap-edit-horizontal",
        "wizard": {
            "title": "Welcome to the Wizard",
            "description": "Please fill things in as you wish",
            "bindings": {
                "name": 1,
                "age": 1,
                "gender": 1,
                "photo": 1,
                "member": 2,
                "phone": 2,
                "icecream": 3,
                "address": 3
            },
            "steps": [{
                "title": "Getting Started",
                "description": "Basic Information"
            }, {
                "title": "Details",
                "description": "Personal Information"
            }, {
                "title": "Preferences",
                "description": "Customize your Profile"
            }]
        }
    }
});
