$(document).ready(function () {

//loading data from backend    
    var backend = function () {
        var jsonTemp = null;
        $.ajax({
            'async': false,
            'url': "https://online-mehr-geschaeft.de/treaction/dev-master-clint-formUI/polling.php",
            'success': function (data) {
                jsonTemp = data;
            }
        });
        return jsonTemp;
    }();

    backend = JSON.parse(backend);


    questions = backend.questions;

    let questionsNumber = questions.length;
//Make all the things under switch cases s fuctions(easy to control events)
//function generateSchema 
// input -------> questions form backend
//output -------> two jsons 1.properties (they merge and go into alpaca function)


    function generateProperties(questions) {
        let questionsNumber = questions.length;

        let properties = {};

        if (questionsNumber > 0) {

            $.each(questions, function (i) {

                let question = questions[i].question_json_representation;

                let type = question.type;

                let ID = questions[i].question_id;

                let propertie = {};

                switch (type) {
                    case 'select':
                        let optionsSelect = {};
                        select = question.select;
                        optionsSelect = select.options;
                        let value = [];
                        $.each(optionsSelect, function (i) {
                            optionValue = optionsSelect[i];
                            goto = optionValue.goto;
                            valuetext = optionValue.value;
                            //make it as array
                            value.push(valuetext);
                        });
                        propertie = {
                            "properties": {
                                [ID]: {
                                    "title": "Select",
                                    "type": "string",
                                    "enum": value
                                }}
                        };
                        break;
                    case 'range':
                        let option = {};
                        range = question.range;
                        option = range.options;
                        valuesRange = option[1];
                        max = parseInt(valuesRange.max);
                        min = parseInt(valuesRange.min);
                        step = parseInt(valuesRange.step);
                        propertie = {
                            "properties": {
                                [ID]: {
                                    "minimum": min,
                                    "maximum": max
                                }}
                        };
                        break;
                    case 'textarea':
                        propertie = {
                            "properties": {
                                [ID]: {
                                    "type": question.type,
                                    "title": question.text
                                }
                            }
                        };
                        break;
                    case 'checkbox':
                        let optionsCheckbox = {};
                        checkbox = question.checkbox;
                        optionsCheckbox = checkbox.options;
                        let valueCheckbox = [];
                        $.each(optionsCheckbox, function (i) {
                            optionValue = optionsCheckbox[i];
                            valuetext = optionValue.value;
                            //make it as array
                            valueCheckbox.push(valuetext);
                        });
                        propertie = {"properties": {
                                [ID]: {
                                    "type": "array",
                                    "items": {
                                        "type": "string",
                                        "enum": valueCheckbox
                                    },
                                    "required": [ID]
                                }}};
                        break;
                    default:
                        alert("There is No question type");
                }
                properties = $.extend(true, {}, propertie, properties);
            });
        } else {
            alert("Schema for alpaca is not generating");
        }

        return properties;
    }
    ;
//This variables are used for mapping next questions
    let surveyMappings = [];
    let pickedValue;
    /*function generateSchema 
     input -------> questions form backend
     output -------> jsons fields (they merge and go into alpaca function)*/
    function generateFields(questions) {
        let questionsNumber = questions.length;
        let fields = {};

        if (questionsNumber > 0) {

            $.each(questions, function (i) {

                let question = questions[i].question_json_representation;
                let ID = questions[i].question_id;

                let type = question.type;

                let field = {};

                switch (type) {
                    case 'select':
                        let optionsSelect = {};
                        select = question.select;
                        optionsSelect = select.options;
                        let valueSelect = [];
                        let surveyMapping = {};
                        $.each(optionsSelect, function (i) {
                            optionValue = optionsSelect[i];
                            goto = optionValue.goto;
                            valuetext = optionValue.value;
                            //making maping
                            if(goto !== ""){
                                surveyMapping = {[ID]: {[valuetext]: [goto]}};
                                surveyMappings.push(surveyMapping);
                            };
                        });
                        let pickedValues;
                        field = {
                            "fields": {
                                [ID]: {
                                    "helper": question.hint,
                                    "onFieldChange": function (e) {
                                        /*pickedValues = this.getValue();
                                        pickedValue = {[ID]: [pickedValues]};
                                        console.log(pickedValue);*/
                                    }
                                }
                            }
                        };
                        break;
                    case 'range':
                        let option = {};
                        range = question.range;
                        option = range.options;
                        valuesRange = option[1];
                        max = parseInt(valuesRange.max);
                        min = parseInt(valuesRange.min);
                        step = parseInt(valuesRange.step);

                        field = {
                            "fields": {
                                [ID]: {
                                    "inputType": "range",
                                    "label": question.text,
                                    "helper": question.hint
                                }
                            }
                        };
                        break;
                    case 'textarea':

                        field = {
                            "fields": {
                                [ID]: {
                                    "type": "textarea",
                                    "helper": question.hint
                                }
                            }
                        };
                        break;
                    case 'checkbox':
                        let optionsCheckbox = {};
                        checkbox = question.checkbox;
                        optionsCheckbox = checkbox.options;
                        let value = [];
                        $.each(optionsCheckbox, function (i) {
                            optionValue = optionsCheckbox[i];
                            valuetext = optionValue.value;
                            goto = optionValue.goto;
                            //make it as array
                            value.push(valuetext);
                        });
                        field = {
                            "fields": {
                                [ID]: {
                                    "label": question.text,
                                    "type": "checkbox",
                                    "optionLabels": value,
                                    "helper": question.hint
                                }}};
                        break;
                    default:
                        alert("There is No question type");
                }
                fields = $.extend(true, {}, field, fields);
            });
        } else {
            console.log("Schema for alpaca is not generating");
        }

        return fields;

    };
console.log(surveyMappings);
//Calling functions:

    let propertieFromFunction = generateProperties(questions).properties;
    let fieldFromFunction = generateFields(questions).fields;
//This variable is used for Branching
//schema for alpaca.
    let schemas = {
        "title": "Test Form for our survey NL",
        "description": "To test visulazation of Dynamic form",
        "type": "object",
        "properties": {}
    };
//seperating properties
    let propertie = {
        "properties": propertieFromFunction
    };
//merging schema with properties

    let schema = $.extend(true, {}, schemas, propertie);


//dependences of properties we need to add this to schema.
    let dependeces = {"dependencies": {
            "propertykey1": ["propertykey2"],
            "propertykey3": ["propertykey4"]
        }};
//order of questions if neded, you need to add this to schema.
    let order = {};

//options for alpacaform.

    let option = {
        "fields": {
            "form": {
                "attributes": {
                    "action": "",
                    "method": "post"
                },
                "buttons": {
                    "submit": {

                    }
                }
            }

        }
    };


//seperating fields
    let field = {
        "fields": fieldFromFunction
    };
//merging fields and options
    let options = $.extend(true, {}, option, field);


    /*functiongenerate steps
     input -------> questions form backend
     output -------> Arrey of steps*/
    function generateSteps(questions) {
        let questionsNumber = questions.length;
        let steps = [];
        if (questionsNumber > 0) {
            $.each(questions, function (i) {
                stepsfill = {};
                steps.push(stepsfill);
            });
        } else {
            console.log("There are No questions");
        }

        return steps;
    }
    ;
    steps = generateSteps(questions);

    /*functiongenerate bindings
     input -------> questions form backend
     output -------> Json of bindings*/
    function generateBindings(questions) {
        let questionsNumber = questions.length;
        let bindings = {};
        if (questionsNumber > 0) {
            $.each(questions, function (i) {
                let ID = questions[i].question_id;
                bindingsFill = {[ID]: i + 1};
                bindings = $.extend(true, {}, bindingsFill, bindings);
            });
        } else {
            console.log("There are No questions");
        }
        return bindings;
    }
    ;
    bindings = generateBindings(questions);

//To generate view for alpaca view.
    let views = {
        "parent": "bootstrap-edit-horizontal",
        "wizard": {
            "title": "Welcome to the Wizard",
            "description": "Please fill things in as you wish",
            "bindings": bindings,
            "steps": steps,
            "showSteps": false,
            "showProgressBar": true
        }
    };
//creating next for mapping
    let next = {
        "validate": function (callback) {
            console.log("Next validate()");
            callback(true);
        },
        "click": function (e) {
            pickedValues = this.getValue();
            //This function returns the value of next
            let nextId = generateNext(pickedValues);
            let id = generateID(questions);
            if (nextId){
                this.trigger("moveToStep", {
                            "index": nextId,
                            "skipValidation": true
                        });
            };
        }
    };
//this variable is to stabilize next
let presentQuestion ;
let previousId ;
//Generate a variable next that question moves
//Function generate next
    function generateNext(pickedValues) {
        let next;
        let values = [];
        console.log(presentQuestion);
        console.log(pickedValues);
        if(typeof presentQuestion === 'undefined'){
            $.each(surveyMappings, function (i) {
                        let value ;
                        id = Object.keys(surveyMappings[i]);
                        if(pickedValues[id]){
                            key = pickedValues[id];
                            previousId = id;
                            value = surveyMappings[i][id][key];
                            if (typeof value === 'undefined' ){
                                value = "";
                            }
                        }
                        values.push(value);
                        });
            //filtering Values arry to get only array            
            var filteredValue = values.filter(Boolean);
            var value = filteredValue[0];
            //changing value to string
            if(typeof value !== "undefined"){
                stringValue = value.toString();
                presentQuestion = stringValue;
                console.log("Skipping question to", stringValue);
                //index starts with 0
                var nextID = stringValue -1;
                return nextID;
            };
        }
    };
//generating questions order in ID from backeend
function generateID(questions){
    let IDs = [];
    $.each(questions, function (i) {
                let question = questions[i].question_json_representation;
                let type = question.type;
                let ID = questions[i].question_id;
                IDs.push(ID);
            });
       return IDs;
};
//previous button
let previous = {
                    "validate": function (callback) {
                        console.log("Previous validate()");
                        callback(true);
                    },
                    "click": function (e) {
                        let previousID = generateprevious();
                        if(previousID){
                            this.trigger("moveToStep", {
                            "index": previousID,
                            "skipValidation": true
                        });
                         setValue: function(val){
                             val = "None";
                         };
                        }
                    }
                };
//function to generate previous Id
function generateprevious(){
    Ids = generateID(questions);
    let previousID;
    if(previousId & typeof presentQuestion === 'string'){
        stringValue = previousId.toString();
         previousID = Ids.indexOf(stringValue);
         previousID = previousID +1;
         presentQuestion = undefined ;
    }else{
        previousID = null;
    };
    return previousID;
};
//Buttons in view.
    let buttons = {
        "wizard": {
            "buttons": {
                "first": {
                    "title": "Go to First Page",
                    "align": "left",
                    "click": function (e) {
                        this.trigger("moveToStep", {
                            "index": 0,
                            "skipValidation": true
                        });
                    }
                },
                "previous": previous,
                "next": next,
                "submit": {
                    "title": "All Done!",
                    "validate": function (callback) {
                        console.log("Submit validate()");
                        callback(true);
                    },
                    "click": function (e) {
                        alert(JSON.stringify(this.getValue(), null, "  "));
                        console.log(JSON.stringify(this.getValue(), null, "  "));
                        $.post("//httpbin.org/post", this.getValue());
                    },
                    "id": "mySubmit",
                    "attributes": {
                        "data-test": "123"
                    }
                }
            }

        }
    };

//Merging buttons and view.
    let view = $.extend(true, {}, views, buttons);

    let data = {};

//Final variable to pass into alpaca.

    let x = {

        "schemaSource": schema,
        "optionsSource": options,
        "viewSource": view,
        "dataSource": data
    };

    console.log(x);

//Alpaca form.
    let y = $("#form").alpaca(x);

});

