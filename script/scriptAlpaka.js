$( document ).ready(function() {
    
    $("#form").alpaca({
    "dataSource": "./data/data.json",
    "schemaSource": "./data/schema.json",
    "optionsSource": "./data/options.json",
    "view": {
        "parent": "bootstrap-edit-horizontal",
        "wizard": {
            "title": "Welcome to the Wizard",
            "description": "Please fill things in as you wish",
            "bindings": {
               "Select": 1,
                "Textbox": 2,
                "Dropdown": 3,
                "Slider":4
            },
            "steps": [{
                "title": "checkbox",
                "description": "Question flow"
            }, {
                "title": "Textbox ",
                "description": "Question flow"
            }, {
                "title": "Dropdown",
                "description": "Question flow"
            },{
                "title": "Slider",
                "description": "Question flow"
            }],
            "showSteps": false,
            "showProgressBar": false,
            "buttons": {
            "first": {
          "title": "Go to First Page",
          "align": "left",
          "click": function(e) {
            this.trigger("moveToStep", {
              "index": 0,
              "skipValidation": true
            });
          }
        },
        "previous": {
          "validate": function(callback) {
            console.log("Previous validate()");
            callback(true);
          }
        },
        "next": {
          "validate": function(callback) {
            console.log("Next validate()");
            callback(true);
          }
        },
        "submit": {
          "title": "All Done!",
          "validate": function(callback) {
            console.log("Submit validate()");
            callback(true);
          },
          "click": function(e) {
            alert(JSON.stringify(this.getValue(), null, "  "));
            $.post( "//httpbin.org/post", this.getValue());
          },
          "id": "mySubmit",
          "attributes": {
            "data-test": "123"
          }
        }
      }
        }
    }
        
     });
    
    
    
});

