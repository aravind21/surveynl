var classcom__maileon__api___maileon_a_p_i_result =
[
    [ "__construct", "classcom__maileon__api___maileon_a_p_i_result.html#ab82f7de6697a13fc81e622b8c16e9ae5", null ],
    [ "getResult", "classcom__maileon__api___maileon_a_p_i_result.html#a9fb110e206557d994562b5640fc06cca", null ],
    [ "getStatusCode", "classcom__maileon__api___maileon_a_p_i_result.html#a70110528dc2bcb3207b68fdcff06825d", null ],
    [ "isSuccess", "classcom__maileon__api___maileon_a_p_i_result.html#ada0762fbdbc29cfde86485b4425f1403", null ],
    [ "isClientError", "classcom__maileon__api___maileon_a_p_i_result.html#a6cba1c76e80b3a810abc850514f48035", null ],
    [ "getContentType", "classcom__maileon__api___maileon_a_p_i_result.html#a81f04e1f0549bd87c5b6c9a4d55f049c", null ],
    [ "getBodyData", "classcom__maileon__api___maileon_a_p_i_result.html#a795eab4751e5cfa3b5fee97473c31278", null ],
    [ "getResultXML", "classcom__maileon__api___maileon_a_p_i_result.html#aa22a008f869ed9c21ffc69e2c4cf99ec", null ],
    [ "getResponseHeaders", "classcom__maileon__api___maileon_a_p_i_result.html#a6d7cff968216ebd896e5a455d8b9b2f9", null ],
    [ "toString", "classcom__maileon__api___maileon_a_p_i_result.html#a6c1243397ba38877505b0762dad03857", null ]
];