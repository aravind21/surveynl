var classcom__maileon__api__reports___conversion =
[
    [ "toString", "classcom__maileon__api__reports___conversion.html#a42de9d196f19c8ade3b2c8acd90a3692", null ],
    [ "fromXML", "classcom__maileon__api__reports___conversion.html#af8547eab4a2a001f92ff7f855d5f7c2e", null ],
    [ "toCsvString", "classcom__maileon__api__reports___conversion.html#ac23900a097e91b886f036a644b4b4a95", null ],
    [ "toXML", "classcom__maileon__api__reports___conversion.html#a722bf9048207983bf1149e5c2c3bed77", null ],
    [ "$timestamp", "classcom__maileon__api__reports___conversion.html#adb673fe0b44f96351dbd597b744c8c76", null ],
    [ "$contactId", "classcom__maileon__api__reports___conversion.html#afb17791884b452ad21b75c5718e91c3a", null ],
    [ "$contactEmail", "classcom__maileon__api__reports___conversion.html#a5d785bf491ebfdd4ae088e0baf002548", null ],
    [ "$value", "classcom__maileon__api__reports___conversion.html#a56a83fbdcd1068a5b3993e3cd1f5bb29", null ],
    [ "$mailingSentDate", "classcom__maileon__api__reports___conversion.html#a3ecccc45544693a11d0dd32d4561175b", null ],
    [ "$mailingId", "classcom__maileon__api__reports___conversion.html#a522e96570de9c10d0feeb473411214b7", null ],
    [ "$mailingName", "classcom__maileon__api__reports___conversion.html#a3860840a43b2d6462fe7a0399b102c8d", null ],
    [ "$siteId", "classcom__maileon__api__reports___conversion.html#adde94f15edfd95a97e598687e3124568", null ],
    [ "$siteName", "classcom__maileon__api__reports___conversion.html#aa6f880df5a13d98bd5d21e8c72f4b084", null ],
    [ "$linkId", "classcom__maileon__api__reports___conversion.html#ac7fd1cb7fbe4f44cccc64b763238763e", null ],
    [ "$linkUrl", "classcom__maileon__api__reports___conversion.html#a98df658ee31f818ed4477cf83c6b950c", null ]
];