var NAVTREE =
[
  [ "Maileon PHP client", "index.html", [
    [ "Overview", "index.html", null ],
    [ "README", "md__r_e_a_d_m_e.html", null ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_abstract_j_s_o_n_wrapper_8php_source.html",
"classcom__maileon__api__contacts___contacts_service.html#af119a14b2881689db3896644835787ae",
"classcom__maileon__api__reports___unique_conversion.html#a25f7fb634b102b32e8a2ba39f7cf52f6"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';