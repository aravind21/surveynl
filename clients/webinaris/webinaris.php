<?php

/*
 *To send data to webinaris
 * @Author Aravind
 */
class webinaris{
    
    public $webinaris;
    private $config;
    private $webinar;
    private $baseurl;
    private $webinarPassword;

    function __construct() {     
        
        $this->config = new config("config.ini");
        $this->webinar = $this->config->getConfigwebinaris();
        $this->webinarId= $this->webinar[webinarID];
        $this->webinarPassword = $this->webinar[APIPassword];
        $this->baseurl = "https://webinaris.co/api/";
	}
    /**
     * sendDataWebinaris:
     * 
     * Sends data to webinarisAPI
     * 
     * @param type $params
     * @return responce, true if sent data to webinarisAPI     
     */ 
        
    function sendDataWebinaris($params){
        
        if($params){
            $config_mailinone = $this->config->getConfigMailInOne();
            $email = $params[$config_mailinone[Mapping][EMAIL]] ;
            $firstname = $params[$config_mailinone[Mapping][FIRSTNAME]] ;
            $lastname = $params[$config_mailinone[Mapping][LASTNAME]] ;
            $time = date("d.m.Y H:i");
            $ip = $params[$config_mailinone[Mapping][ip_adresse]];
            $paid =($this->webinar)? 'YES' : 'NO';
            if($this->webinarId && $this->webinarPassword ){
                $url = $this->baseurl."?key=".$this->webinarId."&is_paid=".$paid."&time=".urlencode($time)."&email=".$email."&firstname=".$firstname."&lastname=".$lastname."&ip_address=".$ip."&password=".$this->webinarPassword;
                $responce =  $this->crulRequest($url);
                return $responce;
            }
            
        }
    }
    /**
     * getDates:
     * 
     * Sends data to webinarisAPI
     * 
     * @return AvaliableDates     
     */ 
    
    function getDates(){
        if(isset($this->webinarId) && isset($this->webinarPassword)){
                $url = $this->baseurl."showtimes?webinaris_id=".$this->webinarId."&api_password=".$this->webinarPassword;
                $responce =  $this->crulRequest($url);
                return $responce;
            }
    }
    
    function crulRequest($url){
        if(isset($url)){
            $ch = curl_init($url);
            $responce = curl_exec($ch);
            return $responce;
            curl_close($ch);
        }
    }
}

?>

