<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require "vendor/autoload.php";

$config = new config('config.ini');
$config_survey = $config->getConfigSurvey();


if($config_survey['Status']){

    $survey_id = $config_survey["id"];
    spl_autoload_register('Autoload::surveyLoader');
    $s_obj = new survey($survey_id);
    //Need to know how this getSurvey function works;
    $survey = $s_obj->getSurvey();
    echo (json_encode($survey));
}


